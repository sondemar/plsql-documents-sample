/* 
@author: Mariusz Sondecki

*/

set linesize 200
set SERVEROUTPUT on

spool log/documents_creation.log

prompt '********************************* Creating tables ********************************'

prompt '********************************* DOCUMENTS ********************************'
@./ddl/tables/DOCUMENTS.sql


prompt '********************************* Creating sequences ********************************'

prompt '********************************* DOCUMENTS_SEQ ********************************'
@./ddl/sequences/DOCUMENTS_SEQ.sql


prompt '********************************* Creating triggers ********************************'

prompt '********************************* DOCUMENTS_INSERT_TRG ********************************'
@./ddl/triggers/DOCUMENTS_INSERT_TRG.sql

prompt '********************************* DOCUMENTS_UPDATE_TRG ********************************'
@./ddl/triggers/DOCUMENTS_UPDATE_TRG.sql


prompt '********************************* Creating packages ********************************'

prompt '********************************* DOCUMENTS_UTIL_SPEC ********************************'
@./ddl/packages/DOCUMENTS_UTIL_SPEC.pks

prompt '********************************* DOCUMENTS_UTIL_BODY ********************************'
@./ddl/packages/DOCUMENTS_UTIL_BODY.pkb

prompt '********************************* DOCUMENTS_IMPORTER_SPEC ********************************'
@./ddl/packages/DOCUMENTS_IMPORTER_SPEC.pks

prompt '********************************* DOCUMENTS_IMPORTER_BODY ********************************'
@./ddl/packages/DOCUMENTS_IMPORTER_BODY.pkb


prompt '********************************* Inserting data for DOCUMENTS ********************************'

prompt '********************************* DOCUMENTS_DATA ********************************'
@./dml/inserts/DOCUMENTS_DATA.sql


spool off
