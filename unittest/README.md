This is extra part responsible for unit testing of PL/SQL
========================================================================================================================
Author: Mariusz Sondecki

System requirements
-------------------

Oracle DB 11g (it can be an Express Edition) and SqlPlus tool and installed REQUIRED unit testing framework utPLSQL: <http://utplsql.sourceforge.net/>

Installation
-------------------------

1. Following with the step 1. from <http://utplsql.sourceforge.net/Doc/fourstep.html> create UTP schema.
		
2. To access procedures of the unit testing framework from own prepared test cases it must be grant execution to own user. Log in to SqlPlus and:		

        SQL>  grant execute on utplsql.test to <USER>;
		
		SQL>  grant execute on utplsql.eq to <USER>;
         		
3. From directory: <plsql-documents-sample\unittest> run testing script via SqlPlus (it will create and then run all test cases):
		
		SQL> @testing.sql

Run test cases
-------------------------

1. From directory: <plsql-documents-sample\unittest> run  script via SqlPlus (it will run all test cases):
		
		SQL> @run.sql