--------------------------------------------------------
--  DDL for Package UT_DOCUMENTS_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "UT_DOCUMENTS_UTIL" AS 

  PROCEDURE ut_setup;
  
  PROCEDURE ut_validate_pesel;
  
  PROCEDURE ut_teardown;

END UT_DOCUMENTS_UTIL;

/
