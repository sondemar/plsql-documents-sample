--------------------------------------------------------
--  DDL for Package Body UT_DOCUMENTS_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "UT_DOCUMENTS_UTIL" AS

  PROCEDURE ut_setup AS
  BEGIN
    NULL;
  END ut_setup;

  PROCEDURE ut_teardown AS
  BEGIN
	-- ROLLBACK;
    NULL;
  END ut_teardown;

  PROCEDURE ut_validate_pesel AS
  BEGIN
    UTP.UTASSERT.eq('correct validation', HR.DOCUMENTS_UTIL.VALIDATE_PESEL(p_pesel => 93052617113), TRUE);
    UTP.UTASSERT.eq('incorrect validation - incorrect checksum', HR.DOCUMENTS_UTIL.VALIDATE_PESEL(p_pesel => 93052617114), FALSE);
    UTP.UTASSERT.eq('incorrect validation - too short', HR.DOCUMENTS_UTIL.VALIDATE_PESEL(p_pesel => 9052617114), FALSE);
  END ut_validate_pesel;

END UT_DOCUMENTS_UTIL;

/
