/* 
@author: Mariusz Sondecki

*/

set linesize 200
set SERVEROUTPUT on

spool log/documents_creation.log

prompt '********************************* Creating packages ********************************'

prompt '********************************* DOCUMENTS_UTIL_SPEC ********************************'
@./tests/UT_DOCUMENTS_UTIL_SPEC.pks

prompt '********************************* DOCUMENTS_UTIL_BODY ********************************'
@./tests/UT_DOCUMENTS_UTIL_BODY.pkb


prompt '********************************* Running Unit Tests ********************************'

@./run.sql


spool off
