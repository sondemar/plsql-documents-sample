--------------------------------------------------------
--  DDL for Table DOCUMENTS
--------------------------------------------------------
DROP TABLE "DOCUMENTS";

CREATE TABLE "DOCUMENTS"
(
	"DOCUMENT_ID"          NUMBER NOT NULL,
	"DOCUMENT_NAME" 	   VARCHAR2(50 BYTE) NOT NULL,
	"DOCUMENT_DESCRIPTION" VARCHAR2(4000 BYTE),
	"RECORD_TIMESTAMP"     TIMESTAMP(6) NOT NULL,
	"TIMESTAMP" 	       TIMESTAMP(6) NOT NULL,
	"RECORD_USER_ID"   	   VARCHAR2(50 BYTE) NOT NULL,
	"USER_ID" 			   VARCHAR2(50 BYTE) NOT NULL
)
;
--------------------------------------------------------
--  Constraints for Table DOCUMENTS
--------------------------------------------------------

ALTER TABLE "DOCUMENTS" ADD CONSTRAINT "DOCUMENTS_PK" PRIMARY KEY ("DOCUMENT_ID");