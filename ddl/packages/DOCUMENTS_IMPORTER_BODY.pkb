--------------------------------------------------------
--  DDL for Package Body DOCUMENTS_IMPORTER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "DOCUMENTS_IMPORTER" AS

  -- ---------------------------------------------------------------------------------------------
  -- @Author: Mariusz Sondecki
  -- @Date: 08/07/2015
  -- @Description:
  -- Procedure creates new database records based on data from nodes included inside p_xml_document.
  -- ---------------------------------------------------------------------------------------------
  PROCEDURE import_nodes_to_records(p_xml_document xmldom.DOMDocument);

  -- ---------------------------------------------------------------------------------------------
  -- @Author: Mariusz Sondecki
  -- @Date: 08/07/2015
  -- @Description:
  -- Implementation
  -- ---------------------------------------------------------------------------------------------
  PROCEDURE read_XML(p_directory VARCHAR2, p_xml_file VARCHAR2) AS
    v_xml_parser xmlparser.parser;
    v_xml_doc xmldom.DOMDocument;
  BEGIN
      v_xml_parser := xmlparser.newParser;

       xmlparser.setValidationMode(v_xml_parser, FALSE);
       xmlparser.setBaseDir(v_xml_parser, p_directory);
       xmlparser.parse(v_xml_parser, p_directory || '\' || p_xml_file);

       v_xml_doc := xmlparser.getDocument(v_xml_parser);
       import_nodes_to_records(v_xml_doc);
    EXCEPTION
      WHEN xmldom.INDEX_SIZE_ERR THEN
         raise_application_error(-20120, 'Index Size error');

      WHEN xmldom.DOMSTRING_SIZE_ERR THEN
         raise_application_error(-20120, 'String Size error');

      WHEN xmldom.HIERARCHY_REQUEST_ERR THEN
         raise_application_error(-20120, 'Hierarchy request error');

      WHEN xmldom.WRONG_DOCUMENT_ERR THEN
         raise_application_error(-20120, 'Wrong doc error');

      WHEN xmldom.INVALID_CHARACTER_ERR THEN
         raise_application_error(-20120, 'Invalid Char error');

      WHEN xmldom.NO_DATA_ALLOWED_ERR THEN
         raise_application_error(-20120, 'Nod data allowed error');

      WHEN xmldom.NO_MODIFICATION_ALLOWED_ERR THEN
         raise_application_error(-20120, 'No mod allowed error');

      WHEN xmldom.NOT_FOUND_ERR THEN
         raise_application_error(-20120, 'Not found error');

      WHEN xmldom.NOT_SUPPORTED_ERR THEN
         raise_application_error(-20120, 'Not supported error');

      WHEN xmldom.INUSE_ATTRIBUTE_ERR THEN
         raise_application_error(-20120, 'In use attr error');
  END read_XML;

  -- ---------------------------------------------------------------------------------------------
  -- @Author: Mariusz Sondecki
  -- @Date: 08/07/2015
  -- @Description:
  -- Implementation
  -- ---------------------------------------------------------------------------------------------
  PROCEDURE import_nodes_to_records(p_xml_document xmldom.DOMDocument) IS
    v_documents_nodes xmldom.DOMNodeList;
    v_len NUMBER;
  
    -- ---------------------------------------------------------------------------------------------
    -- @Author: Mariusz Sondecki
    -- @Date: 08/07/2015
    -- @Description:
    -- Sub-procedure creates a new database record based on data from p_xml_node node.
    -- ---------------------------------------------------------------------------------------------
    PROCEDURE import_node_to_record(p_xml_node xmldom.DOMNode) IS
      v_children_nodes xmldom.DOMNodeList;
      v_xml_element xmldom.DOMElement;
      c_document_name_tag CONSTANT VARCHAR2(13) := 'document_name';
      c_document_desc_tag CONSTANT VARCHAR2(20) := 'document_description';
      v_document_name DOCUMENTS.DOCUMENT_NAME%TYPE;
      v_document_desc DOCUMENTS.DOCUMENT_DESCRIPTION%TYPE;
  
      -- ---------------------------------------------------------------------------------------------
      -- @Author: Mariusz Sondecki
      -- @Date: 08/07/2015
      -- @Description:
      -- Sub-function gets node value in text format.
      -- ---------------------------------------------------------------------------------------------  
      FUNCTION get_node_value(p_xml_nodes xmldom.DOMNodeList) RETURN VARCHAR2 IS
        v_xml_node_value xmldom.DOMNode;
      BEGIN
        -- get the text node associated with the only one element node of the list
        IF NOT xmldom.isNull(p_xml_nodes) AND xmldom.getLength(p_xml_nodes) = 1 THEN
          -- get text value
          v_xml_node_value := xmldom.getFirstChild(xmldom.item(p_xml_nodes, 0));
  
          IF xmldom.getNodeType(v_xml_node_value) = xmldom.TEXT_NODE THEN
            RETURN xmldom.getNodeValue(v_xml_node_value);
          END IF;
        END IF;
  
        RETURN NULL;
      END get_node_value;
    BEGIN
      v_xml_element := xmldom.makeElement(p_xml_node);
      -- get value for <code> c_document_name_tag </code>
      PRAGMA INLINE (get_node_value, 'YES');
      v_document_name := get_node_value(xmldom.getChildrenByTagName(v_xml_element, c_document_name_tag));
      -- get value for <code> c_document_desc_tag </code>
      PRAGMA INLINE (get_node_value, 'YES');
      v_document_desc := get_node_value(xmldom.getChildrenByTagName(v_xml_element, c_document_desc_tag));
  
      INSERT INTO DOCUMENTS(DOCUMENT_NAME, DOCUMENT_DESCRIPTION) VALUES (v_document_name, v_document_desc);
    END import_node_to_record;
  BEGIN
   -- get all <document> tags
    v_documents_nodes := xmldom.getElementsByTagName(p_xml_document, 'document');

   -- loop through <document> tags
   v_len := xmldom.getLength(v_documents_nodes);
   FOR l_document_number IN 0..v_len-1 LOOP
      PRAGMA INLINE (import_node_to_record, 'YES');
      import_node_to_record(xmldom.item(v_documents_nodes, l_document_number));
   END LOOP;

  END import_nodes_to_records;

END DOCUMENTS_IMPORTER;

/
