--------------------------------------------------------
--  DDL for Package Body DOCUMENTS_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "DOCUMENTS_UTIL" AS

  -- ---------------------------------------------------------------------------------------------
  -- @Author: Mariusz Sondecki
  -- @Date: 08/07/2015
  -- @Description:
  -- Implementation
  -- ---------------------------------------------------------------------------------------------
  FUNCTION validate_pesel(p_pesel NUMBER) RETURN BOOLEAN AS
    TYPE t_weight_array IS VARRAY(10) OF SIMPLE_INTEGER;

    -- ordered weight array
    c_weigth CONSTANT t_weight_array := t_weight_array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
    c_len CONSTANT SIMPLE_INTEGER := c_weigth.COUNT;

    v_checksum SIMPLE_INTEGER := 0;
    v_pesel VARCHAR2(11);
  BEGIN
    -- check p_pesel is not null and has only 11 elements
    IF p_pesel IS NULL OR LENGTH(p_pesel) != 11 THEN
      RETURN FALSE;
    END IF;

    v_pesel := TO_CHAR(p_pesel);
    -- check p_pesel has 11 digits
    IF REGEXP_COUNT(p_pesel, '[[:digit:]]') != 11 THEN
      RETURN FALSE;
    END IF;

    FOR i IN 1 .. c_len LOOP
      v_checksum := c_weigth(i) * TO_NUMBER(SUBSTR(v_pesel, i, 1)) + v_checksum;
    END LOOP;

    -- validate checksum
    IF TO_NUMBER(SUBSTR(v_pesel, 11)) != (10 - MOD(v_checksum, 10)) THEN
      RETURN FALSE;
    END IF;

    RETURN TRUE;
  END validate_pesel;

  -- ---------------------------------------------------------------------------------------------
  -- @Author: Mariusz Sondecki
  -- @Date: 08/07/2015
  -- @Description:
  -- Implementation
  -- ---------------------------------------------------------------------------------------------
  PROCEDURE print_documents AS
    CURSOR cur_document IS SELECT DOCUMENT_NAME, DOCUMENT_DESCRIPTION FROM DOCUMENTS;
    TYPE t_documents IS TABLE OF cur_document%ROWTYPE;

    v_documents_tab t_documents;
    v_length NUMBER;
  BEGIN
    OPEN cur_document;
     LOOP
        FETCH cur_document BULK COLLECT INTO v_documents_tab LIMIT 100;

        v_length := v_documents_tab.COUNT;
        FOR i IN 1..v_length LOOP
          IF v_documents_tab(i).DOCUMENT_NAME IS NOT NULL AND TRIM(v_documents_tab(i).DOCUMENT_DESCRIPTION) IS NOT NULL THEN
            dbms_output.put_line('Document name: ' || v_documents_tab(i).DOCUMENT_NAME);
          ELSE
            dbms_output.put_line('No Description');
          END IF;
        END LOOP;

        EXIT WHEN cur_document%NOTFOUND;
     END LOOP;
     CLOSE cur_document;
  EXCEPTION
    WHEN others THEN
      IF cur_document%ISOPEN THEN
        CLOSE cur_document;
      END IF;
      RAISE;
  END print_documents;

END DOCUMENTS_UTIL;

/
