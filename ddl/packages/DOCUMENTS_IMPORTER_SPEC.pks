--------------------------------------------------------
--  DDL for Package DOCUMENTS_IMPORTER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "DOCUMENTS_IMPORTER" AS

  -- ---------------------------------------------------------------------------------------------
  -- @Author: Mariusz Sondecki
  -- @Date: 08/07/2015
  -- @Description:
  -- Procedure read_XML reads p_xml_file file with XML format and imports it to the table DOCUMENTS.
  -- ---------------------------------------------------------------------------------------------
  PROCEDURE read_XML(p_directory VARCHAR2, p_xml_file VARCHAR2);

END DOCUMENTS_IMPORTER;

/
