DECLARE
  P_DIRECTORY VARCHAR2(200);
  P_XML_FILE VARCHAR2(200);
BEGIN
  P_DIRECTORY := 'XMLDIR';
  P_XML_FILE := 'documents.xml';

  DOCUMENTS_IMPORTER.READ_XML(
    P_DIRECTORY => P_DIRECTORY,
    P_XML_FILE => P_XML_FILE
  );
rollback; 
END;
