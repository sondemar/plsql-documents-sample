--------------------------------------------------------
--  DDL for Package DOCUMENTS_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "DOCUMENTS_UTIL" AS

  -- ---------------------------------------------------------------------------------------
  -- @Author: Mariusz Sondecki
  -- @Date: 08/07/2015
  -- @Description:
  -- Function validate_pesel, taking PESEL number as an input parameter, validating it and
  -- returning true if PESEL is correct (calculate control digit) or false if PESEL is not correct.
  -- @Params:
  -- p_pesel PESEL number
  -- @RETURN
  -- true if PESEL is correct or false if PESEL is not correct.
  -- ---------------------------------------------------------------------------------------
  FUNCTION validate_pesel(p_pesel NUMBER ) RETURN BOOLEAN;

  -- ---------------------------------------------------------------------------------------
  -- @Author: Mariusz Sondecki
  -- @Date: 08/07/2015
  -- @Description:
  -- Procedure print_documents. For each record in DOCUMENTS table, check if description is
  -- not empty. If it�s not empty, print document name to the screen (dbms_output). In other
  -- case, print �No Description�.
  -- ---------------------------------------------------------------------------------------
  PROCEDURE print_documents;

END DOCUMENTS_UTIL;

/
