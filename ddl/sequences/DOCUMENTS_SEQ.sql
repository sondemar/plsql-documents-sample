--------------------------------------------------------
--  DDL for Sequence DOCUMENTS_SEQ
--------------------------------------------------------

DROP SEQUENCE  "DOCUMENTS_SEQ";

CREATE SEQUENCE  "DOCUMENTS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
