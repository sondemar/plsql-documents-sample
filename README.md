plsql-documents-sample: Project contains sample procedures/functions and triggers created with Oracle DB 11g which for instance read XML file and populate records or validate PESEL number.
========================================================================================================================
Author: Mariusz Sondecki

Application description
-------------------

1. 'ddl' directory contains packages, sequences, table DOCUMENTS and triggers for table DOCUMENTS to create in the schema.
2. 'dml' directory constains sample data to insert into created table.
3. 'xml' director contains sample XML file with DTD that is used with this sample.
4. 'unittest' directory contains for unit testing of functionality of packages. 
5. Triggers set Record Timestamp  column to current timestamp  only  when  record  is  inserted  for  the  first  time.  The  same  goes  for  Record  User  Id  – currently connected user is inserted when record is created for the first time. Timestamp  and 
User Id  are updated each time both on insert and update to the row. In other words:  Record Timestamp  and  Record  User  Id  columns  must  present  when  and  who  created  the  record  and Timestamp/User Id must present who and when modified the record lately.
6. Function DOCUMENTS_UTIL#validate_pesel(p_pesel NUMBER ), takes PESEL number as an input parameter, validates it and returns true if PESEL is correct (calculate control digit) or false if PESEL is not correct.
7. Procedure DOCUMENTS_UTIL#print_documents for each record in DOCUMENTS table checks if description is not empty. If it’s not empty, print document name to the screen (dbms_output). In other case, print “No Description”.
8. Procedure DOCUMENTS_IMPORTER#read_XML(p_directory VARCHAR2, p_xml_file VARCHAR2) reads file with documents in XML format and imports it to table DOCUMENTS.




System requirements
-------------------

Oracle DB 11g (it can be an Express Edition) and SqlPlus tool

Installation
-------------------------

1. To write log files by Spool tool from <plsql-documents-sample> directory create subdirectory <log> (for log files) and grant the read write permissions for the specified USER (for example PUBLIC).  With the execution command must be provided physical path (for instance c:\\plsql-documents-sample\log). It must be  executed  as SYS or SYSTEM user. Log in to SqlPlus:
		
		C:\>sqlplus / as sysdba

        SQL> create or replace directory LOGDIR as 'c:\plsql-documents-sample\log\';
		
		SQL> grant read, write on directory LOGDIR to PUBLIC;
        
2. To access xml files from DOCUMENTS_IMPORTER package grant the read write permissions for the specified USER (for example PUBLIC) for directory <plsql-documents-sample\xml>.  With execution command must be provided physical path (in this case c:\\plsql-documents-sample\xml). It must be also  executed  like in previous step as SYS or SYSTEM user. Log in to SqlPlus:
		
		C:\>sqlplus / as sysdba

        SQL> create or replace directory XMLDIR as 'c:\plsql-documents-sample\xml\';
		
		SQL> grant read, write on directory XMLDIR to PUBLIC;
         		
3. From directory: <plsql-documents-sample> run installation script via SqlPlus (it will create all objects and data):
		
		SQL> @installation.sql

Run procedures
-------------------------

1. For run procedure DOCUMENTS_IMPORTER.READ_XML can be used sample xml file for which path is: <plsql-documents-sample\xml\documents.xml> and run script which is located in directory <plsql-documents-sample\ddl\packages\run_scripts> with name <test_DOCUMENTS_IMPORTER_readXML.sql> and content: 

		DECLARE
		  P_DIRECTORY VARCHAR2(200);
		  P_XML_FILE VARCHAR2(200);
		  P_ERROR_FILE VARCHAR2(200);
		BEGIN
		  P_DIRECTORY := 'XMLDIR';
		  P_XML_FILE := 'documents.xml';

		  DOCUMENTS_IMPORTER.READ_XML(
			P_DIRECTORY => P_DIRECTORY,
			P_XML_FILE => P_XML_FILE,
		  );
		--rollback; 
		END;

